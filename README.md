# StatusPageCli

Welcome to Status Page Cli, the Dev Assessment for Vehiculum by `Gaston Trujillo Pulgar`! 

## Installation

Clone this repo in your folder. Make sure You're using Ruby `2.4.5` (Can be installed with RVM)

After having the repo cloned and the ruby version installed, go to the directory and run


    $ bundle

And install it yourself as:


    $ sudo chmod +x exe/status_page (to allow execution)
    $ bundle exec rake build
    $ gem install `pkg/status_page_cli-0.2.0.gem` (for now it can be used just as the local build of the gem)

## Usage

From the repo folder cloned:

- 1) Get data from service pages `status_page pull`
  - `--site https://somesite.com` to run for specific site
  - `--with_output` to save to default file `./status_page_pull_data.json` and print results
  - `--file file_name` to define custom file
- 2) Get your pull command running for certain amount of time `status_page live` 
  - `--every n` being n the custom amount of time in seconds 
- 3) Display historic data `status_page history` 
  - `--file file_name` to load history from given file 
- 4) Backup your data `status_page backup` 
  - `--source file_name` to backup given file 
  - `--path file_name` to define backup destination (required flag) 
- 5) Restore your backup data `status_page restore` 
  - `--path file_name` to restore backup from given file
- 6) Get some stats from your historic data `status_page stats` 
- 7) Search through your historic data `status_page search`
  - `--file file_name` to search from given file
  - `--name Page Name` to search by name 
  - `--description status` to search by status
  - `--url` to search by URL 
   
## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Test

Run all testing site by calling `rspec`

OR

Run integration testing by calling `bundle exec cucumber features/`

## Libraries / Gems dependencies

- `Rspec` & `cucumber/aruba` for unit and feature testing
- `RestClient` to run HTTP requests to status sites
- `Thor` to build Cli-like interface
- `Colorize` to allow printing to terminal with colors
- `Terminal-table` to build a full ASCII table to display data
- `Timers` to run time-based events (used for live command)
- `Time-difference` to display time stats in a human readable way
- `Webmock` to mock HTTP requests (Used for testing)
- `Byebug` to enable debugging


## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/gtrujillop/status_page_cli. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the StatusPageCli project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/gtrujillop/status_page_cli/blob/master/CODE_OF_CONDUCT.md).
