RSpec.describe StatusPageCli::Options::Pull do
  include_context 'shared_requests'

  let(:extractor) { StatusPageCli::Extractor }
  let(:file_path) { 'pull_test.txt' }
  before do
    urls.each_with_index do |url, idx|
      stub_request(:get, url).to_return(body: responses[idx], status: 200)
    end
  end
  describe '#save_to_file' do
    it 'calls extractor index method' do
      expect(extractor).to receive(:index)
      described_class.new(extractor).save_to_file(file_path)
    end
    it 'saves to given file' do
      described_class.new(extractor).save_to_file(file_path)
      data = File.readlines(file_path).map do |line|
        JSON.parse(line)
      end.flatten
      ['GitHub', 'Atlassian Bitbucket', 'Cloudflare', 'RubyGems.org'].each_with_index do |name, idx|
        expect(data[idx]['page']['name']).to eq name
      end
    end
  end
  describe '#print_table' do
    it 'calls a new Terminal::Table instance' do
      expect(Terminal::Table).to receive(:new)
      described_class.new(extractor).print_table
    end
    
  end
  
  after { File.delete(file_path) if File.exist?(file_path) }
end
