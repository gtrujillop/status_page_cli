RSpec.shared_context "shared_requests" do
  let(:urls) {
    %w(
      https://www.githubstatus.com/api/v2/status.json
      https://bitbucket.status.atlassian.com/api/v2/status.json
      https://www.cloudflarestatus.com/api/v2/status.json
      https://status.rubygems.org/api/v2/status.json
    )
  }
  let(:responses) {
    [
      "{\"page\":{\"id\":\"kctbh9vrtdwd\",\"name\":\"GitHub\",\"url\":\"https://www.githubstatus.com\",\"time_zone\":\"Etc/UTC\",\"updated_at\":\"2019-04-06T20:11:54.307Z\"},\"status\":{\"indicator\":\"none\",\"description\":\"All Systems Operational\"}}",
      "{\"page\":{\"id\":\"bqlf8qjztdtr\",\"name\":\"Atlassian Bitbucket\",\"url\":\"https://bitbucket.status.atlassian.com\",\"time_zone\":\"Etc/UTC\",\"updated_at\":\"2019-04-06T20:14:01.632Z\"},\"status\":{\"indicator\":\"none\",\"description\":\"All Systems Operational\"}}",
      "{\"page\":{\"id\":\"yh6f0r4529hb\",\"name\":\"Cloudflare\",\"url\":\"https://www.cloudflarestatus.com\",\"time_zone\":\"Etc/UTC\",\"updated_at\":\"2019-04-06T20:15:05.050Z\"},\"status\":{\"indicator\":\"minor\",\"description\":\"Minor Service Outage\"}}",
      "{\"page\":{\"id\":\"pclby00q90vc\",\"name\":\"RubyGems.org\",\"url\":\"https://status.rubygems.org\",\"time_zone\":\"Etc/UTC\",\"updated_at\":\"2019-04-06T20:15:05.073Z\"},\"status\":{\"indicator\":\"none\",\"description\":\"All Systems Operational\"}}"
    ]
  }
end