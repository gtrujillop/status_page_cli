RSpec.describe StatusPageCli::Extractor do
  include_context 'shared_requests'
  describe '.index' do
    context 'when response is successful' do
      before do
        urls.each_with_index do |url, idx|
          stub_request(:get, url).to_return(body: responses[idx], status: 200)
        end
      end
      it 'returns JSON with all responses from URLs' do
        result = described_class.index
        ['GitHub', 'Atlassian Bitbucket', 'Cloudflare', 'RubyGems.org'].each_with_index do |name, idx|
          expect(result[idx]['page']['name']).to eq name
        end
      end
    end
    context 'when response is not successful' do
      before do
        urls.each_with_index do |url, idx|
          stub_request(:get, url).to_return(body: responses[idx], status: 404)
        end
      end
      it 'returns empty array' do
        expect(described_class.index).to be_empty
      end
    end
    
  end
end
