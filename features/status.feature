Feature: Status
  In order to get status from service pages
  As a CLI
  I want to be as objective as possible

  Scenario: Ping pong
    When I run `status_page ping`
    Then the output should contain "pong"

  Scenario: Pull
    When I run `status_page pull`
    Then the output should contain "getting status from"

  Scenario: Pull with output
    When I run `status_page pull --with_output`
    Then the output should contain "getting status from"
    And the output should contain "GitHub"
    And the output should contain "https://www.githubstatus.com"
    And the output should contain "All Systems Operational"

  Scenario: Live
    When I run `status_page live`
    Then the output should contain "Running every 10 seconds"
    And the output should contain "GitHub"
    And the output should contain "https://www.githubstatus.com"
    And the output should contain "All Systems Operational"

  Scenario: Live with timer
    When I run `status_page live --every 5`
    Then the output should contain "Running every 5 seconds"
    And the output should contain "GitHub"
    And the output should contain "https://www.githubstatus.com"
    And the output should contain "All Systems Operational"

  Scenario: Search by name
    When I run `status_page pull`
    Then I run `status_page search --name GitHub`
    And the output should contain "GitHub"
    And the output should contain "https://www.githubstatus.com"
    And the output should contain "All Systems Operational"
    And the output should not contain "Attlasian Bitbucket"

  Scenario: Search by status description
    When I run `status_page pull`
    Then I run `status_page search --description outage`
    And the output should not contain "Cloudflare"
    And the output should not contain "GitHub"