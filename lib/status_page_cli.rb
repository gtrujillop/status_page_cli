require 'byebug'

require "status_page_cli/helpers"
require "status_page_cli/version"
require "status_page_cli/cli"
require "status_page_cli/extractor"
require "status_page_cli/options/pull"
require "status_page_cli/options/live"
require "status_page_cli/options/history"
require "status_page_cli/options/backup"
require "status_page_cli/options/restore"
require "status_page_cli/options/stats"
require "status_page_cli/options/aggregator_dsl"

module StatusPageCli
  class ExtractorError < StandardError; end
  class UrlError < StandardError; end
  class FileError < IOError; end
end
