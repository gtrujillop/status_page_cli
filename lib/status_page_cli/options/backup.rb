module StatusPageCli
  module Options
    class Backup

      attr_reader :pull, :source

      def initialize(pull, source)
        @pull = pull
        @source = source
      end
      
      def run(filename)
        puts "Pulling latest data for backup \n"
        pull.save_to_file(source)
        puts "Saving backup to #{filename} \n"
        IO.copy_stream(source, filename)
        puts "Backup data saved to #{filename} \n"
      rescue => e
        puts "Error while generating backup file #{e.message}"
      end
    end
  end
end
