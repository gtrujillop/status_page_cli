require 'json'

module StatusPageCli
  module Options
    class AggregatorDSL
      include StatusPageCli::Helpers

      attr_accessor :data, :conditions, :results
      attr_reader :file

      def initialize(file, conditions = {})
        @file = file
        @data = load_data
        @conditions = conditions
        @results = []
      end
      
      def where(property, expected)
        conditions[property] = expected
      end

      def method_missing(m, *args, &block)
        where(m, args.first)
      end

      def query(&block)
        instance_eval(&block)
        search
      end

      def flush
        conditions = {}
      end
      
      private

      def search
        # [
        #   {
        #     "page": {
        #         "id": "kctbh9vrtdwd",
        #         "name":"GitHub",
        #         "url":"https://www.githubstatus.com",
        #         "time_zone":"Etc/UTC",
        #         "updated_at":"2019-04-05T04:26:40.314Z"
        #       },
        #     "status": {
        #       "indicator":"none",
        #       "description":"All Systems Operational"
        #     }
        #   }
        # ]
        unless conditions.empty?
          conditions.each do |key, value|
            results << data.find_all do |row|
              condition_value_included?(row, key, value)
            end
          end
          print_table(results.flatten)
        end
      end

      def condition_value_included?(record, field, value)
        record.dig('page', field.to_s)&.downcase&.include?(value) || record.dig('status', field.to_s)&.downcase&.include?(value)
      end
      
      def load_data
        File.readlines(file).map do |line|
          next if line.empty?
          JSON.parse(line)
        end.flatten!
      rescue => e
        puts "Error while reading file #{e.message}"
      end

      def print_table(results)
        table = Terminal::Table.new do |t|
          t << ['Site Name', 'Url', 'Updated At', 'Status', 'Requested At']
          t << :separator
          results.each do |site|
            t << [site['page']['name'], site['page']['url'], site['page']['updated_at'], color_status(site['status']['description']), Time.now]
          end
        end
        puts table
      end
      
    end
  end
end
