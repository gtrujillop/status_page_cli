module StatusPageCli
  module Options
    class Restore
      
      def self.run(path, destination)
        puts "Restoring backup from #{path} to #{destination} \n"
        IO.copy_stream(path, destination)
        puts "Backup data restored to #{destination} \n"
      rescue => e
        puts "Error while generating backup file #{e.message}"
      end
    end
  end
end
