module StatusPageCli
  module Options
    class Live

      attr_reader :pull, :filename

      def initialize(pull, filename)
        @pull = pull
        @filename = filename
      end

      def run
        pull.print_table
        pull.save_to_file(filename)
      end
    end
  end
end