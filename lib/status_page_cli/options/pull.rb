require 'json'

module StatusPageCli
  module Options
    class Pull
      include StatusPageCli::Helpers

      attr_accessor :site
      attr_reader :extractor

      def initialize(extractor)
        @extractor = extractor
        @site = nil
      end

      def save_to_file(filename)
        f = File.new(filename, 'a')
        f.write("#{data.to_json}\n")
        puts "Status data saved to #{filename} \n"
        f.close
      rescue => e
        puts "Error while saving file #{e.message}"
      end
      
      private

      def data
        @data ||= extractor.index(site)
      end
      
    end
  end
end