require 'json'

module StatusPageCli
  module Options
    class History
      include StatusPageCli::Helpers

      attr_accessor :data, :file

      def initialize(file)
        @file = file
        @data = []
      end
      
    end
  end
end
