require 'json'
require 'time'
require 'time_difference'

module StatusPageCli
  module Options
    class Stats
      include StatusPageCli::Helpers

      attr_accessor :data, :file

      def initialize(file)
        @file = file
        @data = []
      end
      
      def print_stats
        table = Terminal::Table.new do |t|
          t << ['Site Name', 'Up Since', 'Down time']
          t << :separator
          calculate_stats.each do |site, stats|
            t << [site, stats['up_from'], stats['down_time']]
          end
        end
        puts table
      end

      alias_method :generate, :load

      private

      def calculate_stats
        data.each_with_object({}) do |line, hash|
          hash[line['page']['name']] = {}.tap do |value|
            statuses = by_service_name(line).map { |row| row['status']['description'] }
            timestamps = by_service_name(line).map { |row| row['page']['updated_at'] }
            indexes = indexes_by_status(statuses)
            value['up_from'] = uptime(timestamps, indexes) || 'None'
            value['down_time'] = downtime(timestamps, indexes) || 'None'
          end
        end
      end
  
      def by_service_name(line)
        data.select do |row| 
          row['page']['name'] == line['page']['name']
        end
      end

      def indexes_by_status(statuses)
        statuses.uniq.each_with_object({}) do |status, hash|
          hash[status] = statuses.each_index.select { |i| statuses[i] == status } 
        end
      end

      def uptime(timestamps, indexes)
        return nil unless indexes['All Systems Operational']
        start_time = Time.parse(timestamps[indexes['All Systems Operational'][0]])
        end_time = Time.now.utc
        TimeDifference.between(start_time, end_time).humanize
      end

      def downtime(timestamps, indexes)
        return nil unless indexes['Minor Service Outage']
        latest_outage_time = timestamps[indexes['Minor Service Outage'][-1]] # From latest outage detected
        if latest_outage_time == timestamps.last
          start_time = Time.parse(latest_outage_time) 
          end_time = Time.now.utc
        else
          start_time = Time.parse(latest_outage_time)
          end_time = Time.parse(timestamps[indexes['All Systems Operational'][-1]])
        end
        TimeDifference.between(start_time, end_time).humanize
      end
    end
  end
end
