require 'thor'
require 'colorize'
require 'terminal-table'
require 'timers'

require 'status_page_cli'

module StatusPageCli
  class Cli < Thor
    DEFAULT_FILE = './status_page_pull_data.json'.freeze
    desc "responds with pong", "> status_page ping pong"
    def ping
      puts "pong"
    end
    
    desc "pull all the status data from pages", "pull --with_output --file data.json"
    option :with_output
    option :site
    option :file
    def pull
      if options[:site]
        pull_operator.site = options[:site]
      end
      if options[:with_output]
        pull_operator.print_table
      end
      pull_operator.save_to_file(file)  
    end

    desc "gathers data periodically every N seconds", "> status_page live --every 1"
    option :every
    def live
      live_option = StatusPageCli::Options::Live.new(pull_operator, DEFAULT_FILE)
      timers = Timers::Group.new
      if options[:every]
        puts "Running every #{options[:every]} seconds. Stop with CMD + C \n"
        live_option.run
        timers.every(options[:every].to_i) { live_option.run }
      else
        puts "Running every 10 seconds. Stop with CMD + C \n"
        live_option.run
        timers.every(10) { live_option.run }
      end
      loop { timers.wait }
    end

    desc "loads all historic data from default or given path", "> status_page history --file here.json"
    option :file
    def history
      history_operator = StatusPageCli::Options::History.new(file)
      puts "Loading records from #{file} \n"
      proc = Proc.new { history_operator.print_table }
      history_operator.load proc
    end

    desc "saves a backup of historic and current data on given path", "> status_page backup --path /some/path/here.json"
    option :source
    option :path, required: true
    def backup
      source = options[:source] ? options[:source] : DEFAULT_FILE
      backup_operator = StatusPageCli::Options::Backup.new(pull_operator, source)
      backup_operator.run(options[:path])
    end

    desc "restores existing backup into default file", "> status_page restore --path /some/path/here.json"
    option :path, required: true
    def restore
      restore_operator = StatusPageCli::Options::Restore
      restore_operator.run(options[:path], DEFAULT_FILE)
    end

    desc "calculates and display stats from history", "> status_page stats --file here.json"
    option :file
    def stats
      stats_operator = StatusPageCli::Options::Stats.new(file)
      puts "Loading records from #{file} \n"
      proc = Proc.new { stats_operator.print_stats }
      stats_operator.generate proc
    end

    desc "runs field-based search through historic data", "> status_page search --file here.json"
    option :file
    option :name
    option :url
    option :description
    def search
      aggregator_dsl = StatusPageCli::Options::AggregatorDSL.new(file)
      query_opts = options.except(:file)
      query_opts.each { |k, v| query_opts[k] = v.downcase }
      aggregator_dsl.query do
        query_opts.each do |k, v|
          send(k.to_sym, v)
        end
      end
    end
    
    private

    def pull_operator
      @pull_operator ||= StatusPageCli::Options::Pull.new(StatusPageCli::Extractor)
    end

    def file
      options[:file] ? options[:file] : DEFAULT_FILE
    end
  end
end
