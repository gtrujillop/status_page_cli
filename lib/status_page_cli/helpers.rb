module StatusPageCli
  module Helpers

    def print_table
      table = Terminal::Table.new do |t|
        t << ['Site Name', 'Url', 'Updated At', 'Status', 'Requested At']
        t << :separator
        data.each do |site|
          t << [site['page']['name'], site['page']['url'], site['page']['updated_at'], color_status(site['status']['description']), Time.now]
        end
      end
      puts table
    end

    def load(proc)
      File.readlines(file).each do |line|
        next if line.empty?
        data << JSON.parse(line)
      end
      data.flatten!
      proc.call
    rescue => e
      puts "Error while reading file #{e.message}"
    end
    
    private

    def color_status(status)
      return status.colorize(:red) if status.downcase.include?("down")
      return status.colorize(:green) if status.downcase.include?("all systems operational")
      return status.colorize(:yellow) if status.downcase.include?("minor service outage")
    end
  end
end
