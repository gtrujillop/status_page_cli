require 'rest-client'
require 'json'
require 'open-uri'

module StatusPageCli
  class Extractor
    URLS = %w(
      https://www.githubstatus.com/api/v2/status.json
      https://bitbucket.status.atlassian.com/api/v2/status.json
      https://www.cloudflarestatus.com/api/v2/status.json
      https://status.rubygems.org/api/v2/status.json
    )
    DEFAULT_ENDPOINT = '/api/v2/status.json'

    def self.index(site = nil)
      sites = validate_url(site) || URLS
      sites.map do |url|
        RestClient.get(url) do |response, request|
          resp = JSON.parse(response)
          case response.code
          when 200
            puts "getting status from #{url}"
            resp
          when 400..510
            puts "Error while retrieving data from #{url}"
            return []
          end
        end
      end
    rescue => e
      puts "Error while retrieving data: #{e.message} \n"
      return []
    end

    def self.validate_url(url = nil)
      return unless url
      parsed_url = URI.parse(url)
      case parsed_url
      when URI::HTTP || URI::HTTPS
        url += DEFAULT_ENDPOINT unless url.include?(DEFAULT_ENDPOINT)
        return [url.downcase]
      else
        puts 'Given site is not a valid url.'
      end
    end
    
  end
    
end
